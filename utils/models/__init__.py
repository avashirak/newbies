from .abstract import AbstractUUID, AbstractTimeTrackable
from .singleton import SingletonModel
