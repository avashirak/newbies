from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework import serializers

from users.models import User


class UserGetResponsibleSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = User
        fields = ('responsiblein')


class UserDetailsSerialiazer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('uuid', 'first_name', 'last_name', 'email',)
        read_only_fields = ('uuid', 'first_name', 'last_name', 'email',)
