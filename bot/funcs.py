from typing import Tuple
from newbies.models import Novice


async def add_user(first_name, last_name, email, education, about_me, chat_id) -> Tuple[Novice]:
    return await Novice.objects.create(
        chat_id=chat_id,
        first_name=first_name,
        last_name=last_name,
        email=email,
        education=education,
        about_me=about_me,
    )
