import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
os.environ.setdefault('DJANGO_ALLOW_ASYNC_UNSAFE', "true")
django.setup()

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from .config import *
from .funcs import *


class NoviceStates(StatesGroup):
    first_name = State()
    last_name = State()
    email = State()
    education = State()
    about_me = State()
    chat_id = State()


@dp.message_handler(commands=["start"], state='*')
async def start_handler(message: types.Message):
    print("Bot started")
    await NoviceStates.first_name.set()
    text = """Введите свое имя."""
    await message.answer(text)


@dp.message_handler(state=NoviceStates.first_name)
async def first_name_reg(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text.isalpha():
            data['chat_id'] = message.chat.id
            data['first_name'] = message.text
            text = """Введите фамилию."""
            await message.answer(text)
            await NoviceStates.last_name.set()
        else:
            text = """Введите свое имя."""
            await message.answer(text)


@dp.message_handler(state=NoviceStates.last_name)
async def last_name_reg(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text.isalpha():
            data['last_name'] = message.text
            text = """Введите свою почту."""
            await message.answer(text)
            await NoviceStates.email.set()
        else:
            text = """Введите фамилию."""
            await message.answer(text)


@dp.message_handler(state=NoviceStates.email)
async def email_reg(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text.isprintable():
            data['email'] = message.text
            text = """Введите название университета/колледжа и специальность."""
            await message.answer(text)
            await NoviceStates.education.set()
        else:
            text = """Введите свою почту."""
            await message.answer(text)


@dp.message_handler(state=NoviceStates.education)
async def educ_reg(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text.isprintable():
            data['education'] = message.text
            text = """Напишите коротко о себе."""
            await message.answer(text)
            await NoviceStates.about_me.set()
        else:
            text = """Введите название университета/колледжа и специальность."""
            await message.answer(text)


@dp.message_handler(state=NoviceStates.about_me)
async def aboutme_reg(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        try:
            if message.text.isprintable():
                data['about_me'] = message.text

                Novice.objects.create(
                    first_name=data['first_name'],
                    last_name=data['last_name'],
                    email=data['email'],
                    education=data['education'],
                    about_me=data['about_me'],
                    chat_id=data['chat_id'],
                )
                print("Novice created")

                await state.finish()

                text = """Вы занесены в список."""
                await message.answer(text)
            else:
                text = """Напишите коротко о себе."""
                await message.answer(text)
        except:
            text = """Вы уже зарегистрированы."""
            await state.finish()
            await message.answer(text)


