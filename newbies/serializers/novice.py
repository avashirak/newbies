from rest_framework import serializers
from rest_framework.validators import ValidationError

from newbies.models import Novice


class NoviceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Novice
        fields = ('uuid', 'created_at', 'first_name', 'last_name', 'email')


class NoviceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Novice
        fields = (
                  'uuid',
                  'created_at',
                  'updated_at',
                  'first_name',
                  'last_name',
                  'email',
                  'age',
                  'education',
                  'about_me',
                  'chat_id',
                  )


class NoviceUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Novice
        fields = ('__all__')


class NoviceRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Novice
        fields = ('__all__')