import asyncio
from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework.validators import ValidationError

from newbies.models import NoviceGroup

from newbies.tasks import notification_created_group


class GroupSerializer(FlexFieldsModelSerializer):
    # expandable_fields = {
    #     "responsible": (UserGetResponsibleSerializer, {"source": "responsible"}),
    # }
    # responsible = UserGetResponsibleSerializer()

    class Meta:
        model = NoviceGroup
        fields = (
            'uuid',
            'name',
            'novices',
            'responsible'
        )


class GroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoviceGroup
        fields = (
            'uuid',
            'name',
            'novices',
            'responsible'
        )

    def create(self, validated_data):
        novices = validated_data.get('novices')
        if NoviceGroup.check_count(novices) is False:
            raise ValidationError({'non_fields_error': ['Число участников выше чем максимально допустимое']})
        novice = validated_data.pop('novices') if 'novices' in validated_data else []
        obj = NoviceGroup.objects.create(**validated_data)
        obj.novices.set(novice)

        return obj


