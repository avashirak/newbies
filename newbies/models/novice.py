from django.db import models
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import User

from utils.models import AbstractUUID, AbstractTimeTrackable


class Novice(AbstractUUID, AbstractTimeTrackable):
    first_name = models.CharField(
        _("Имя"),
        max_length=64
    )
    last_name = models.CharField(
        _("Фамилия"),
        max_length=64
    )
    email = models.EmailField(
        _("Почта")
    )
    age = models.PositiveIntegerField(
        _("Возраст"),
        blank=True,
        null=True,
    )
    education = models.CharField(
        _("Образование"),
        max_length=128,
    )
    about_me = models.TextField(
        _("Обо мне"),
        max_length=256,
        blank=True,
        null=True,
    )
    chat_id = models.CharField(
        _("Уникальный номер чата"),
        max_length=128,
        unique=True,
        null=True
    )

    class Meta:
        verbose_name = _("Новичок")
        verbose_name_plural = _("Новички")

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


