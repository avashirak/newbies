from django.conf.global_settings import AUTH_USER_MODEL
from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import AbstractUUID, AbstractTimeTrackable
from newbies.models.groupsettings import GroupSettings
from users.models import User


class NoviceGroup(AbstractUUID, AbstractTimeTrackable, models.Model):
    name = models.CharField(
        _("Название"),
        max_length=128
    )
    responsible = models.ForeignKey(User, verbose_name=_("Ответственный"),
                                    related_name="novice_group", on_delete=models.CASCADE, null=True)

    novices = models.ManyToManyField('newbies.Novice', verbose_name=_("Новички"),
                                     related_name="groups", blank=True)

    class Meta:
        verbose_name = _("Группа Новичков")
        verbose_name_plural = _("Группы Новичков")

    def __str__(self):
        return f'{self.name}'

    @staticmethod
    def check_count(novices) -> bool:
        return len(novices) <= GroupSettings.load().max_members
