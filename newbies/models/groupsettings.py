from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import SingletonModel


class GroupSettings(SingletonModel):
    max_members = models.PositiveSmallIntegerField(
        _("Максимальное количество участников группы"),
        default=7,
    )
