import logging

from celery import shared_task
from newbies.models import Novice, NoviceGroup
from bot.config import bot


@shared_task
async def notification_created_group(uuid: str):
    group = NoviceGroup.objects.get(pk=uuid)
    novices = group.novices.all()
    for i in novices:
        await bot.send_message(chat_id=i.chat_id,
                               text=f"Вы были добавлены в группу {group.name}.\nОтветственный за вашу группу: {group.responsible}.\nЧлены вашей группы: {str([i.first_name for i in novices])[1:-1]}")

