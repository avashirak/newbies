from django.contrib import admin
from .models import NoviceGroup, Novice, GroupSettings

# Register your models here.
admin.site.register(Novice),
admin.site.register(NoviceGroup),
