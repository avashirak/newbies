import asyncio

from rest_flex_fields import FlexFieldsModelViewSet
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_nested.viewsets import NestedViewSetMixin

from newbies.models import Novice, NoviceGroup, GroupSettings
from users.models import User
from users.serializers import UserDetailsSerialiazer
from newbies.serializers import (
    NoviceUpdateSerializer,
    NoviceRetrieveSerializer,
    NoviceListSerializer,
    NoviceCreateSerializer,
    GroupCreateSerializer,
    GroupSerializer
)
from newbies.tasks import *


class NoviceViewSet(viewsets.ModelViewSet):
    queryset = Novice.objects.all()
    serializer_class = NoviceCreateSerializer

    def get_serializer_class(self):
        serializer = self.serializer_class

        if self.action == 'list':
            serializer = NoviceListSerializer
        elif self.action == 'retrieve':
            serializer = NoviceRetrieveSerializer
        elif self.action == 'update':
            serializer = NoviceUpdateSerializer

        return serializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = NoviceGroup.objects.all().prefetch_related('novices')
    permit_list_expands = ["responsible"]
    serializer_class = GroupSerializer

    # def get_serializer_class(self):
    #     serializer = self.serializer_class
    #
    #     if self.action == 'create':
    #         serializer = GroupCreateSerializer
    #
    #     return serializer

    def create(self, request, *args, **kwargs):
        serializer = GroupCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        asyncio.run(notification_created_group(serializer.data['uuid']))
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserDetailsSerialiazer

