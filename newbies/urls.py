from rest_framework import routers
from django.urls import include, path

from newbies.views import NoviceViewSet, GroupViewSet, UserViewSet


router = routers.DefaultRouter()
router.register(r'novices', NoviceViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
]

