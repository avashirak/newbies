from django.apps import AppConfig


class NewbiesConfig(AppConfig):
    name = 'newbies'
